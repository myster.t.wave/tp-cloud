# TP2 : Déploiement cloud



🌞 **Une fois que c'est fait, pour vérifier la bonne installatation...**


```bash
az>> az group create --name efreitp2 --location francecentral
{
  "id": "/subscriptions/a8133d66-8132-4122-b93b-78875d059d7a/resourceGroups/efreitp2",
  "location": "francecentral",
  "managedBy": null,
  "name": "efreitp2",
  "properties": {
    "provisioningState": "Succeeded"
  },
  "tags": null,
  "type": "Microsoft.Resources/resourceGroups"
}
```
# Partie 1 : Premiers pas Azure

# I. Premiers pas

🌞 **Créez une VM depuis la WebUI**

 ```
PS C:\Users\Bingo> ssh ha@40.113.40.26
The authenticity of host '40.113.40.26 (40.113.40.26)' can't be established.
ECDSA key fingerprint is SHA256:eej3eS4p/LJRHcP958ADlyi7gjI/k9hOo4be0rMQnYU.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '40.113.40.26' (ECDSA) to the list of known hosts.
Welcome to Ubuntu 22.04.3 LTS (GNU/Linux 6.2.0-1018-azure x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

 System information disabled due to load higher than 1.0

Expanded Security Maintenance for Applications is not enabled.

0 updates can be applied immediately.

Enable ESM Apps to receive additional future security updates.
See https://ubuntu.com/esm or run: sudo pro status


The list of available updates is more than a week old.
To check for new updates run: sudo apt update


The programs included with the Ubuntu system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Ubuntu comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
applicable law.

To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.
```

🌞 **Créez une VM depuis le Azure CLI**


```bash
az>> vm create --name oe --resource-group VMAZ --image Ubuntu2204 --admin-username haha --ssh-key-values "C:\Users\Bingo\.ssh\id_rsa.pub"
{
  "fqdns": "",
  "id": "/subscriptions/a8133d66-8132-4122-b93b-78875d059d7a/resourceGroups/VMAZ/providers/Microsoft.Compute/virtualMachines/oe",
  "location": "northeurope",
  "macAddress": "00-22-48-9D-6F-DB",
  "powerState": "VM running",
  "privateIpAddress": "10.0.0.5",
  "publicIpAddress": "20.234.115.25",
  "resourceGroup": "VMAZ",
  "zones": ""
}
az>>
```
```
PS C:\Users\Bingo> ssh haha@20.234.115.25
The authenticity of host '20.234.115.25 (20.234.115.25)' can't be established.
ECDSA key fingerprint is SHA256:d9lpdxzAl7k9jxIh8MDs4nCAY4cFwqlJYptHv5+lpdc.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '20.234.115.25' (ECDSA) to the list of known hosts.
Welcome to Ubuntu 22.04.3 LTS (GNU/Linux 6.2.0-1018-azure x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Tue Jan  9 15:03:13 UTC 2024

  System load:  0.0458984375      Processes:             101
  Usage of /:   5.1% of 28.89GB   Users logged in:       0
  Memory usage: 7%                IPv4 address for eth0: 10.0.0.5
  Swap usage:   0%

Expanded Security Maintenance for Applications is not enabled.

0 updates can be applied immediately.

Enable ESM Apps to receive additional future security updates.
See https://ubuntu.com/esm or run: sudo pro status


The list of available updates is more than a week old.
To check for new updates run: sudo apt update


The programs included with the Ubuntu system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Ubuntu comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
applicable law.

To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.
```

🌞 **Créez deux VMs depuis le Azure CLI**

````
haha@oe:~$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:22:48:9d:6f:db brd ff:ff:ff:ff:ff:ff
    inet 10.0.0.5/24 metric 100 brd 10.0.0.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::222:48ff:fe9d:6fdb/64 scope link
       valid_lft forever preferred_lft forever
haha@oe:~$ ping 10.0.0.4
PING 10.0.0.4 (10.0.0.4) 56(84) bytes of data.
64 bytes from 10.0.0.4: icmp_seq=1 ttl=64 time=0.889 ms
64 bytes from 10.0.0.4: icmp_seq=2 ttl=64 time=1.89 ms
64 bytes from 10.0.0.4: icmp_seq=3 ttl=64 time=1.07 ms
^C
--- 10.0.0.4 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2002ms
rtt min/avg/max/mdev = 0.889/1.282/1.888/0.434 ms
haha@oe:~$
````
````
haha@oeoe:~$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:0d:3a:67:6c:b5 brd ff:ff:ff:ff:ff:ff
    inet 10.0.0.4/24 metric 100 brd 10.0.0.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::20d:3aff:fe67:6cb5/64 scope link
       valid_lft forever preferred_lft forever
haha@oeoe:~$ ping 10.0.0.5
PING 10.0.0.5 (10.0.0.5) 56(84) bytes of data.
64 bytes from 10.0.0.5: icmp_seq=1 ttl=64 time=0.984 ms
64 bytes from 10.0.0.5: icmp_seq=2 ttl=64 time=1.33 ms
64 bytes from 10.0.0.5: icmp_seq=3 ttl=64 time=1.43 ms
^C
--- 10.0.0.5 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 0.984/1.249/1.433/0.192 ms
haha@oeoe:~$
````

# II. cloud-init

```yml
#cloud-config
users:
  - name: <Theo>
    primary_group: <TPEfrei>
    groups: sudo
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    lock_passwd: false
    passwd: <$6$f9/7Nl2FYdOWhFiE$w/2vKq9HYpmsohAnbD0Eu2McCoTlp5vMlQ0rurQUrYrpinSyZfOTUuLpeRrVMLebaAgADSB7ethmOIPJ0IGz91>
    ssh_authorized_keys:
      - <ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCneVki/Gcoosks1xS77bwAkxdCAWT1Qw7XWQQNdT0VG5IgfwToQdA889dHn++PTUlNnoEW8YIH2QSkWODt+VYETs9WcQRyRBXdNlqieKzXBC5jmQYtvO7pjd2vTj2JIdGEp4o6W8tptRUtcEaeuKxrL4UJi8n4PzEGhRU1WHcFJMyp27dZjzl4Jex7pdxe1xhQwxILkqDpmJF0zrDxjN59WOG5S8rTNhsqhiDBFfJ3Wj/NLIHOWWRj4dmKybv29EHqYRrG6KoHYc/F2wovLWHL+Knt9Uciq85g+lZFttV4oTobhcPHodiCyyfsKX3hA4QpBB8Uy+F7vgvQ/p04ATNg4h+q7uOY3xkxw1cdJPdTnZ1IvN9uol1+wJPz9polD9oXhqJdvBaeztL5WDUyFXDblv4ajCgV06ZE2m+14tXARZsZs0VjwLORFVJZGhYM6F2n+kaR2MYWvAR/EnIOmoG2HEtU5YUc8dv/qZdBmbhf1EvLGTTzpD/6ZljcRer9Kv3k66W0dWlJ9rdPPFe/DfS7AathddfmOtu0LYPtV25HW0zNSKEcL+pOnXuLTnQL2eScQroxUV08+fc5MBrKrqKJxHAt71kA8mILx+hhl3eMfqB42jFt4jnDeMtv4YEtjojuyw4VZaDBij4WKqHVQ34SG12uH2l6nhYvViqhmiubvQ== bingo@DESKTOP-DBPDN91>

```


🌞 **Tester `cloud-init`**

````
az>> vm create --name oeoeoe --resource-group VMAZ --image Ubuntu2204 --admin-username haha --ssh-key-values "C:\Users\Bingo\.ssh\id_rsa.pub" --custom-data C:\Users\Bingo\Desktop\cloud\cloud-init.txt
{
  "fqdns": "",
  "id": "/subscriptions/a8133d66-8132-4122-b93b-78875d059d7a/resourceGroups/VMAZ/providers/Microsoft.Compute/virtualMachines/oeoeoe",
  "location": "northeurope",
  "macAddress": "00-0D-3A-B9-43-0A",
  "powerState": "VM running",
  "privateIpAddress": "10.0.0.5",
  "publicIpAddress": "4.231.206.152",
  "resourceGroup": "VMAZ",
  "zones": ""
}
az>>
````
🌞 **Vérifier que `cloud-init` a bien fonctionné**


